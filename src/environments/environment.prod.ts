export const environment = {
  production: true,
  serverUrl: 'https://ionshop.mimados.pet/parse',
  appUrl: 'https://ionshop.mimados.pet',
  appImageUrl: 'https://ionshop.mimados.pet/assets/imgs/ionshop.png',
  appId: 'HdGUFMh0cg',
  fbId: '2081754298556977',
  stripePublicKey: 'pk_test_MVWLbqwGEHDv5J9MkdN4i7Ot',
  androidHeaderColor: '#5D67D4',
  defaultLang: 'en',
};
